package p�quer;

import java.util.ArrayList;

public class Pocker extends Jugada{

	@Override
	protected Integer cantidadVistos(Carta carta, ArrayList<Carta> cartas) {
		int cantVistos = 0;
		for (Carta i: cartas) {
			cantVistos += esIgual(i.getValor(), carta.getValor());
		}
		
		return cantVistos;
	}
	
	private int esIgual(int i, int valor) {
		if(i == valor) {
			return 1;
		}
		else {return 0;}
	}

	@Override
	protected String es(ArrayList<Integer> valor) {
		 if (valor.contains(4)){return "Es Pocker";}
		 else {return "";}
	}

	@Override
	protected Integer valor(ArrayList<Carta> jugador) {
		if (this.esJugada(jugador) == "Es Pocker") {
			return 100;
		}
		else {return 0;}
	}



}
