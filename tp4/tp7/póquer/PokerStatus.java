package p�quer;

import java.util.ArrayList;

public class PokerStatus {
//	private ArrayList<Integer> valores = new ArrayList<Integer>();
//	private ArrayList<String> color = new ArrayList<String>();
	private ArrayList<Jugada> jugadas = agregarJugadas();

	private ArrayList<Carta> listaDeCartas(Carta x, Carta y, Carta z, Carta p, Carta q) {
		ArrayList<Carta> cartas = new ArrayList<Carta>();
		cartas.add(x);
		cartas.add(y);	
		cartas.add(z);
		cartas.add(p);
		cartas.add(q);
		return cartas;
	}
	
	private ArrayList<Jugada> agregarJugadas() {
		ArrayList<Jugada> lista = new ArrayList<Jugada>();
		lista.add(new Pocker());
		lista.add(new Trio());
		lista.add(new Color());
		return lista;
	}

	public String verificar (Carta x, Carta y, Carta z, Carta p, Carta q) {
		String tipo = "";
		for (Jugada jugada : jugadas) {
			tipo = tipo + jugada.esJugada(listaDeCartas(x, y, z, p, q));
		}
		return tipoDeJuego(tipo);
	}

	private String tipoDeJuego(String tipo) {
		if (tipo.isEmpty()) {
			System.out.println("No es Nada");
			return "No es Nada";
			}
		else {
			System.out.println(tipo);
			return tipo;
			}
	}
	
	public int puntos (ArrayList<Carta> jugador) {
		int tipo = 0;
		for (Jugada jugada : jugadas) {
			tipo = tipo + jugada.valor(jugador);
		}
		return tipo;
	}
	
	
	
	
}
