package p�quer;

import java.util.ArrayList;

public class Color extends Jugada {

	@Override
	protected Integer cantidadVistos(Carta carta, ArrayList<Carta> cartas) {
		int cantVistos = 0;
		for (Carta i: cartas) {
			cantVistos += esIgualColor(i.getPalo(), carta.getPalo());
		}
		
		return cantVistos;
	}
	
	private int esIgualColor(String i, String color) {
		if (colorDe(i) == colorDe(color)) {
			return 1;
		}
		else { return 0; }
	}
	private String colorDe(String palo) {
		if (palo == "P" || palo == "T"  ) {
			return "Negro";
		}
		else {return "Roja";}
	}

	@Override
	protected String es(ArrayList<Integer> valor) {
		 if (valor.contains(5)){
			 return "Es Color";
			 }
		 else {return "";}
		}

	@Override
	protected Integer valor(ArrayList<Carta> jugador) {
		if (this.esJugada(jugador) == "Es Color") {
			return 10;
		}
		else {return 0;}
	}
}
