package p�quer;

import java.util.ArrayList;

public abstract class Jugada {
	public String esJugada(ArrayList<Carta> cartas) {
		ArrayList<Integer> cantValoresRepetidos = new ArrayList<Integer>();
		for (Carta carta : cartas) {
			cantValoresRepetidos.add(this.cantidadVistos(carta, cartas));
		}
		
		return es(cantValoresRepetidos); 
	}

	protected abstract String es(ArrayList<Integer> cantValoresRepetidos);

	protected abstract Integer cantidadVistos(Carta carta, ArrayList<Carta> cartas);
	
//	public int puntos(ArrayList<Carta> jugador) {
//		return valor(jugador.get(0), jugador.get(1), jugador.get(2), jugador.get(3), jugador.get(4));
//	}

	protected abstract Integer valor(ArrayList<Carta> jugador);
	

}
