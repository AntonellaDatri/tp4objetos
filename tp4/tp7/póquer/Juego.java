package p�quer;


import java.util.ArrayList;


public class Juego {
	PokerStatus juego = new PokerStatus(); 

	public Object ganadorEntre(ArrayList<Carta> jugador1, ArrayList<Carta> jugador2) {
		int puntosJ1 = juego.puntos(jugador1);
		int puntosJ2 = juego.puntos(jugador2);
		if (puntosJ1 > puntosJ2 ) {
			return jugador1;
		}
		else if (puntosJ1 < puntosJ2 ) {
			return jugador2;
		}
		else {
			System.out.println("empate");
			return verificarJugada(jugador1, jugador2); 
		}
	}

	private Object verificarJugada(ArrayList<Carta> jugador1, ArrayList<Carta> jugador2) {
		int j1 = juego.puntos(jugador1);
		if (j1 >= 100 || j1 == 1) {
			return laMayorJugada(jugador1, jugador2);
		}
		else {return laMayorSuma(jugador1, jugador2);}
	}

	private Object laMayorSuma(ArrayList<Carta> jugador1, ArrayList<Carta> jugador2) {
		if ((sumaCartas(jugador1)) > (sumaCartas(jugador1))) {	
			return jugador1;
		}
		else {return jugador2;}
	}
	
	private int sumaCartas(ArrayList<Carta> jugador) {
		int total = 0;
		for (Carta c : jugador) {
			total = total + c.getValor();
		}
		return total;
	}

	private Object laMayorJugada(ArrayList<Carta> jugador1, ArrayList<Carta> jugador2) {
		if ((sumaCartasIguales(jugador1)) > (sumaCartasIguales(jugador2))) {	
			return jugador1;
		}
		else {return jugador2;}
	}

	private int sumaCartasIguales(ArrayList<Carta> jugador) {
		ArrayList<ArrayList<Carta>> cartasOrd = cartasOrdenadas(jugador);
		ArrayList<Carta> cartaMax = cartasOrd.get(0);
		for (ArrayList<Carta> carta: cartasOrd) {
			cartaMax = mayorCantidad(cartaMax, carta);
		}
		
		return cartaMax.get(0).getValor();
	}

	private ArrayList<Carta> mayorCantidad(ArrayList<Carta> cartaMax, ArrayList<Carta> carta) {
		if (cartaMax.size() > carta.size()) {
			return cartaMax;
		}
		else {
			return carta;		
		}
	
	}

	private ArrayList<ArrayList<Carta>> cartasOrdenadas(ArrayList<Carta> jugador) {
		ArrayList<ArrayList<Carta>> ordenada = new ArrayList<ArrayList<Carta>>();
		for (Carta carta : jugador) {
			ordenada = ordenar(carta,ordenada);
		}
		return ordenada;
	}

	private ArrayList<ArrayList<Carta>> ordenar(Carta carta, ArrayList<ArrayList<Carta>> ordenada) {
		ArrayList<ArrayList<Carta>> ordenada2 = new ArrayList<ArrayList<Carta>>();
		for (ArrayList<Carta> cartas : ordenada) {
			ordenada2.add(siCorresponede(carta, cartas));		
		}
		ArrayList<Carta> listaCarta = new ArrayList<Carta>();
		listaCarta.add(carta);
		ordenada2.add(listaCarta);
		return ordenada2;
		
	}
	
	private ArrayList<Carta> siCorresponede(Carta carta, ArrayList<Carta> cartas) {
		if (cartas.get(0).getValor() == carta.getValor()) {
			cartas.add(0, carta);
			return cartas;
		}
		else {return cartas;}
	}
/*
   double max = 0;
        for (int i = 0; i < t.size(); i++) {
            if (t.get(i) > max) {
                max = t.get(i);
            }

        System.out.println("M�ximo: " + max);
 
 * protected Integer cantidadVistos(Carta carta, ArrayList<Carta> cartas) {
		int cantVistos = 0;
		for (Carta i: cartas) {
			cantVistos += esIgual(i.getValor(), carta.getValor());
		}
		
		return cantVistos;
	}
	
	private int esIgual(int i, int valor) {
		if(i == valor) {
			return 1;
		}
		else {return 0;}
	}*/

}

	
