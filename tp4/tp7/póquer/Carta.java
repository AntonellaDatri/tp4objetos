package p�quer;

public class Carta {
	private int valor;
	private String palo;
	
	public int getValor() { 
		return valor;
	}

	public String getPalo() {
		return palo;
	}

	public Carta(int valor, String palo) {
		super();
		this.valor = valor;
		this.palo = palo;
	}
}
