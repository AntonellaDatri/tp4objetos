package tp8;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito.Then;

import wikipedia.*;

class Wikipedia {
	CommonLink link;
	CommonProperty property;
	SameInitialLetter letter;
	Page page;
	Page page1;
	Page page2;
	Page page3;
	Page page4;
	Page page5;
	
	
	@BeforeEach
	public void setUp() {
		link = new CommonLink();
		property = new CommonProperty();
		letter = new SameInitialLetter();
		
		List<WikipediaPage> list = new LinkedList<WikipediaPage>();
		list.add(page4);
		//
		Map<String, WikipediaPage> mapComun = new HashMap<String, WikipediaPage>();
		mapComun.put("Tierra", page4);
		
		Map<String, WikipediaPage> map = new HashMap<String, WikipediaPage>();
		map.put("Frutilla", page4);
		
		page = new Page("Tierra", list, mapComun);		
		page1 = new Page("Tiramisu", list, map);
		page2 = new Page("Ti-rex", new LinkedList<WikipediaPage>(), mapComun);
		page3 = new Page("Frutilla", list, mapComun);
		page4 = mock(Page.class);
		when (page4.getTittle()).thenReturn("Tierra");
		
		link.addPage(page1);
		link.addPage(page2);
		link.addPage(page3);
		property.addPage(page1);
		property.addPage(page2);
		property.addPage(page3);
		letter.addPage(page1);
		letter.addPage(page2);
		letter.addPage(page3);

		
		
	}
	
	@Test
	void MismaLetraInicial() {
		assertFalse(letter.filtrar(page).contains(page3));
		assertTrue(letter.filtrar(page).contains(page1) && letter.filtrar(page).contains(page2));
	}
	
	@Test
	void LinkEnComun() { 
		assertFalse(link.filtrar(page).contains(page3));
		assertTrue(link.filtrar(page).contains(page1) && link.filtrar(page).contains(page2));
	}
	
	@Test
	void PropiedadEnComun() {
		assertFalse(property.filtrar(page).contains(page1));
		assertTrue(property.filtrar(page).contains(page3) && property.filtrar(page).contains(page2));
	}

}
