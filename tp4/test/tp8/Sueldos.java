package tp8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import sueldos.*;


class Sueldos {
	Empresa empresa;
	Temporario temporario;
	Pasante pasante;
	Planta planta;
	
	@BeforeEach
	public void setUp() {
		empresa = new Empresa();
		temporario = new Temporario(192);
		pasante = new Pasante(30);
		planta = new Planta(20);
		temporario.hijos(2);
		planta.hijos(2);
		empresa.agregarEmpleado(temporario);
		empresa.agregarEmpleado(pasante);
		empresa.agregarEmpleado(planta);
	}
		
	@Test
	void SueldosDeEmpresa() {
		temporario.tieneConjugue();
		assertEquals(empresa.pagoDeSueldos(), 5881.2);
	}
	
	@Test
	void SueldoDeUnTemporario() {
		temporario.tieneConjugue();
		assertEquals(temporario.sueldo(), 1966.2);
	}
	
	@Test
	void SueldoDeUnTemporarioNuevoHijo() {
		temporario.tuvoUnHijo();
		assertEquals(temporario.sueldo(), 1966.2);
	}
	
	@Test
	void SueldoDeUnPasante() {
		assertEquals(pasante.sueldo(), 1044);
	}
	
	@Test
	void SueldoDeUnPlanta() {
		assertEquals(planta.sueldo(), 2871);
	}
}
