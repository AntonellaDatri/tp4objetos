package pokerStatus;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import p�quer.Carta;
import p�quer.PokerStatus;

class MokitoTest {
	Carta c1;
	Carta c2;
	Carta c3;
	Carta c4;
	Carta c5;
	PokerStatus pokerStatus;
	
	
	@BeforeEach
	public void setUp() {
		pokerStatus = new PokerStatus();
		c1 = mock(Carta.class);
		c2 = mock(Carta.class);
		c3 = mock(Carta.class);
		c4 = mock(Carta.class);
		c5 = mock(Carta.class);
		
		when (c1.getPalo()).thenReturn("T");
		when (c2.getPalo()).thenReturn("T");
		when (c3.getPalo()).thenReturn("C");
		when (c4.getPalo()).thenReturn("T");
		when (c5.getPalo()).thenReturn("D");
		when (c1.getValor()).thenReturn(10);
		when (c2.getValor()).thenReturn(10);
		when (c3.getValor()).thenReturn(5);
		when (c4.getValor()).thenReturn(3);
		when (c5.getValor()).thenReturn(6);
	}
	
	@Test
	void noEsNada() {
		assertEquals(pokerStatus.verificar(c1, c2, c3, c4, c5), "No es Nada");
	}
	
	@Test
	void hayPoker() {
		when (c4.getValor()).thenReturn(10);
		when (c5.getValor()).thenReturn(10);
		assertEquals(pokerStatus.verificar(c1, c2, c3, c4, c5), "Es Pocker");
	}
	
	@Test
	void esTrio() {
		when (c4.getValor()).thenReturn(10);
		assertEquals(pokerStatus.verificar(c1, c2, c3, c4, c5), "Es Trio");
	}
	
	@Test
	void esColor() {
		when (c3.getPalo()).thenReturn("P");
		when (c5.getPalo()).thenReturn("P");
		assertEquals(pokerStatus.verificar(c1, c2, c3, c4, c5), "Es Color");
	}
	

}
