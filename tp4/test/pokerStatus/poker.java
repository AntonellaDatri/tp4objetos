package pokerStatus;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import p�quer.Carta;
import p�quer.Juego;
import p�quer.PokerStatus;
 
//import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class poker {
	//setUp
	Juego juego;
	PokerStatus pokerStatus;	
	Carta P2;
	Carta T2;
	Carta CA;
	Carta DA;
	Carta TA;
	Carta CK;
	Carta DK;
	Carta TK;
	Carta P3;
	Carta P5;
	Carta P10;
	Carta C10;
	Carta D10;
	Carta T10;
	
	
	@BeforeEach
	public void setUp() {
		juego = new Juego();
		pokerStatus = new PokerStatus();
		P2 = new Carta(2, "P");
		P3 = new Carta(3, "P");
		T2 = new Carta(2, "T");
		CA = new Carta(13, "C");
		DA = new Carta(13, "D");
		TA = new Carta(13, "T");
		CK = new Carta(12, "C");
		DK = new Carta(12, "D");
		TK = new Carta(12, "T");
		P5 = new Carta(5, "P");
		P10 = new Carta(10, "P");
		C10 = new Carta(10, "C");
		D10 = new Carta(10, "D");
		T10 = new Carta(10, "T");
	}
	 
	//Verify
	@Test
	void noEsNada() {
		assertEquals(pokerStatus.verificar(P3, P2, T10, T2, D10), "No es Nada");
	}
	@Test
	void hayPoker() {
		assertEquals(pokerStatus.verificar(P10, C10, T10, P5, D10), "Es Pocker");
	}
	@Test
	void faltaUnoParaPoker() {
		assertEquals(pokerStatus.verificar(P10, P5, P3, C10, T10), "Es Trio");
	}
	
	@Test
	void esTrio() {
		assertEquals(pokerStatus.verificar(CA, DA, TA, T2, P3), "Es Trio");
	}

	@Test
	void esColor() {
		assertEquals(pokerStatus.verificar(P10, T10, P2, P5, P3), "Es Color");
	}
	
	@Test
	void esColorConDiferentesPalos() {
		assertEquals(pokerStatus.verificar(P10, P10, T2, P5, P3), "Es Color");
	}
	
	@Test
	void P() {
		ArrayList<Carta> lista = new ArrayList<Carta>();
		lista.add(C10);
		lista.add(null);
		lista.add(null);
		System.out.println(lista);
		
	}
	@Test
	void Jugador1GanadorEmpate() {
		ArrayList<Carta> jugador1 = new ArrayList<Carta>();
		jugador1.add(CA);
		jugador1.add(DA);
		jugador1.add(TA);
		jugador1.add(T2);
		jugador1.add(P3);
		ArrayList<Carta> jugador2 = new ArrayList<Carta>();
		jugador2.add(CK);
		jugador2.add(DK);
		jugador2.add(TK);
		jugador2.add(P10);
		jugador2.add(P5);
		assertEquals(juego.ganadorEntre(jugador1, jugador2), jugador1);
	}
	
	@Test
	void Jugador2GanadorPokerColor() {
		ArrayList<Carta> jugador1 = new ArrayList<Carta>();
		jugador1.add(P5);
		jugador1.add(TK);
		jugador1.add(TA);
		jugador1.add(P3);
		jugador1.add(P2);
		ArrayList<Carta> jugador2 = new ArrayList<Carta>();
		jugador2.add(C10);
		jugador2.add(D10);
		jugador2.add(T10);
		jugador2.add(P10);
		jugador2.add(CA);
		assertEquals(juego.ganadorEntre(jugador1, jugador2), jugador2);
	}
	 
	@Test
	void Jugador1GanadorColor() {
		ArrayList<Carta> jugador1 = new ArrayList<Carta>();
		jugador1.add(P5);
		jugador1.add(TK);
		jugador1.add(TA);
		jugador1.add(P3);
		jugador1.add(P2);
		ArrayList<Carta> jugador2 = new ArrayList<Carta>();
		jugador2.add(C10);
		jugador2.add(DA);
		jugador2.add(T10);
		jugador2.add(P10);
		jugador2.add(CA);
		assertEquals(juego.ganadorEntre(jugador1, jugador2), jugador1);
	}
	
	@Test
	void Jugador2GanadorNormal() {
		ArrayList<Carta> jugador1 = new ArrayList<Carta>();
		jugador1.add(P5);
		jugador1.add(TK);
		jugador1.add(CA);
		jugador1.add(P3);
		jugador1.add(P2);
		ArrayList<Carta> jugador2 = new ArrayList<Carta>();
		jugador2.add(CA);
		jugador2.add(DA);
		jugador2.add(TA);
		jugador2.add(T2);
		jugador2.add(P3);
		assertEquals(juego.ganadorEntre(jugador1, jugador2), jugador2);
	}

}
