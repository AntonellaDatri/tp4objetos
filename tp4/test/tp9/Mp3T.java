package tp9;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import mp3.*;

class Mp3T {

	Mp3 mp3;
	Cancion cancion1;
	Cancion cancion2;
	
	@BeforeEach
	void setUp() throws Exception {
		mp3 =new Mp3();
		cancion1 = new Cancion();
		cancion2 = new Cancion();
		mp3.parar(cancion1);
	}
	
	@Test
	void reproducir() {
		assertEquals(mp3.reproducir(cancion1), "reproduciendo");
	}
	@Test
	void reproducirFalloPausa() {
		mp3.reproducir(cancion2);
		mp3.pausar(cancion2);
		assertEquals(mp3.reproducir(cancion1), "Error");
	}
	@Test
	void reproducirFallo() {
		mp3.reproducir(cancion2);
		assertEquals(mp3.reproducir(cancion1), "Error");
	}
	
	
	@Test
	void pararReproduccion() {
		mp3.reproducir(cancion2);
		assertEquals(mp3.parar(cancion1), "parar");
	}
	@Test
	void pararPausa() {
		mp3.reproducir(cancion2);
		mp3.pausar(cancion2);
		assertEquals(mp3.parar(cancion1), "parar");
	}
	@Test
	void pararFallo() {
		assertEquals(mp3.parar(cancion1), "No Hice Nada");
	}
	
	
	@Test
	void pausar() {
		mp3.reproducir(cancion2);
		assertEquals(mp3.pausar(cancion1), "pausado");
	}
	@Test
	void pausarEnPusa() {
		mp3.reproducir(cancion2);
		mp3.pausar(cancion2);
		assertEquals(mp3.pausar(cancion1), "reproduciendo");
	}
	@Test
	void pausarFallo() {
		assertEquals(mp3.pausar(cancion1), "Error");
	}

}
