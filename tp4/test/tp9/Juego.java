package tp9;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import juego.*;

class Juego {
	Maquina maquina;
	
	@BeforeEach
	void setUp() throws Exception {
		maquina = new Maquina();
	}

	@Test
	void unJugador() {
		maquina.agragarFicha();
		assertEquals(maquina.Inicio(), "Juega 1");
	}
	@Test
	void dosJugadores() {
		maquina.agragarFicha();
		maquina.agragarFicha();
		assertEquals(maquina.Inicio(), "Juega 2");
	}
	@Test
	void inicio() {
		assertEquals(maquina.Inicio(), "Ingresar Fichas");
	}

}
