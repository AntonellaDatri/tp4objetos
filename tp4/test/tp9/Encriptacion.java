package tp9;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import encriptación.*;

class Encriptacion {
	EncriptadorNaive encriptador;
	Vocales vocal;
	Numeros numero;

	@BeforeEach
	public void setUp() {
	encriptador= new EncriptadorNaive();
	vocal = new Vocales();
	numero = new Numeros();
	}

	@Test
	void VolcalesE() {
		encriptador.setTipo(vocal);
		assertEquals(encriptador.encriptar("murcielago") , "marcoilegu");
	}
	@Test
	void VolcalesD() {
		encriptador.setTipo(vocal);
		assertEquals(encriptador.desencriptar("marcoilegu") , "murcielago");
	}

	@Test
	void NumerosE() {
		encriptador.setTipo(numero);
		assertEquals(encriptador.encriptar("diego"), "4,9,5,7,16,");
	}
	@Test
	void NumerosD() {
		encriptador.setTipo(numero);
		assertEquals(encriptador.desencriptar("4,9,5,7,16"), "diego");
	}
}
