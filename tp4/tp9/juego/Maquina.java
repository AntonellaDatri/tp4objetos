package juego;

public class Maquina {
	
	int jugadores = 0;
	CantJugadores unJugador = new UnJugador();
	CantJugadores dosJugadores = new DosJugadores();
	
	public String Inicio() {
		if (jugadores == 1) {
			jugadores = 0;	
			return unJugador.jugar();
		}
		else if (jugadores == 0) {
			return "Ingresar Fichas";
		}
		else {
			jugadores = 0;
			return dosJugadores.jugar();
		}
		
	}

	public void agragarFicha() {
		jugadores += 1;
		
	}

}
