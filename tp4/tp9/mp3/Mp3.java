package mp3;

public class Mp3 {
	Estado seleccion = new Seleccion();
	Estado pausa = new Pausa();
	Estado reproduccion = new Reproduccion();
	Estado estado = seleccion;
	
	public void cambiarASeleccion() {
		estado = seleccion;
	}
	
	public void cambiarAPausa() {
		estado = pausa;
	}
	
	public void cambiarAReproduccion() {
		estado = reproduccion;
	}
	
	public String reproducir(Cancion cancion) {
		return estado.play(cancion, this);
	}

	public String pausar(Cancion cancion) {
		return estado.pause(cancion, this);
		
	}
	public String parar (Cancion cancion) {
		return estado.stop(cancion, this);
		
	}
}
