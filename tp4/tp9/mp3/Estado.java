package mp3;

public interface Estado {
	
	
	public String play(Cancion cancion, Mp3 mp3);
	public String pause(Cancion cancion, Mp3 mp3);
	public String stop(Cancion cancion, Mp3 mp3);
}
