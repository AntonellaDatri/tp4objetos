package mp3;

public class Seleccion implements Estado {

	@Override
	public String play(Cancion cancion, Mp3 mp3) {
		mp3.cambiarAReproduccion();
		return cancion.play();
	}

	@Override
	public String pause(Cancion cancion, Mp3 mp3) {
		return "Error";
	}

	@Override
	public String stop(Cancion cancion, Mp3 mp3) {
		return "No Hice Nada";
	}

}
