package mp3;

public class Reproduccion implements Estado {
	
	@Override
	public String play(Cancion cancion, Mp3 mp3) {
		return "Error";
	}

	@Override
	public String pause(Cancion cancion, Mp3 mp3) {
		mp3.cambiarAPausa();
		return cancion.pause();
	}

	@Override
	public String stop(Cancion cancion, Mp3 mp3) {
		mp3.cambiarASeleccion();
		return cancion.stop();
	}

}
   