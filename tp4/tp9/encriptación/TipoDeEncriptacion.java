package encriptación;

public interface TipoDeEncriptacion {

	public String encriptar(String texto);

	public String desencriptar(String texto);
	
	
}
