package encriptación;


import java.util.ArrayList;

public class Numeros implements TipoDeEncriptacion {

	@Override
	public String encriptar(String texto) {
		String nuevoTexto = "";
		int tamañoDelTexto = texto.length();
		while (tamañoDelTexto != 0) {
			nuevoTexto = (Integer.toString(letras().indexOf(texto.substring((tamañoDelTexto-1), tamañoDelTexto)))) + ","  + nuevoTexto;
			tamañoDelTexto = tamañoDelTexto -1;
		}
		return nuevoTexto;
	} 
 
	@Override
	public String desencriptar(String texto) {
		String nuevoTexto = "";
		ArrayList<ArrayList<String>> textoSeparado = textoSeparado(texto);
		int tamañoDelTexto = textoSeparado.size();
		while (tamañoDelTexto != 0) {
			tamañoDelTexto = tamañoDelTexto -1;
			nuevoTexto = (valores( textoSeparado.get(tamañoDelTexto).get(0) )  + nuevoTexto );
		
		} 
		return nuevoTexto;
	}
	public ArrayList<ArrayList<String>> textoSeparado(String texto) {
		ArrayList<ArrayList<String>> text = new ArrayList<ArrayList<String>>();
		text.add(new ArrayList<String>());
		int tamañoDelTexto = texto.length();
		
		while (tamañoDelTexto != 0) {
			agregar(text, texto.substring((tamañoDelTexto-1), tamañoDelTexto));
			tamañoDelTexto = tamañoDelTexto -1;
		}
		return text;
		
	}

	private void agregar(ArrayList<ArrayList<String>> text, String string) {
		if (string.equals(",")) {
			text.add(0, new ArrayList<String>());
		}
		else {
			text.get(0).add(0, string);
			sumarLosUltimos(text);}
		
	} 

	private void sumarLosUltimos(ArrayList<ArrayList<String>> text) {
		String texto = "";
		for (String s : text.get(0)) {
			texto += s;
		}
		text.get(0).clear();
		text.get(0).add(texto);
		
	}

	public String valores(String string) {
		return (letras().get(Integer.parseInt(string))) ;
	}
	public ArrayList<String> letras(){ 
		ArrayList<String> letras = new ArrayList<String>();
		letras.add(" ");
		letras.add("a"); 
		letras.add("b");
		letras.add("c");
		letras.add("d");
		letras.add("e");
		letras.add("f");
		letras.add("g");
		letras.add("h");
		letras.add("i");
		letras.add("j");
		letras.add("k");
		letras.add("l");
		letras.add("m");
		letras.add("n");
		letras.add("ñ");
		letras.add("o");
		letras.add("p");
		letras.add("q");
		letras.add("r");
		letras.add("s");
		letras.add("t");
		letras.add("u");
		letras.add("v");
		letras.add("w");
		letras.add("y");
		letras.add("x");
		letras.add("z");
		return letras;
	}

}
