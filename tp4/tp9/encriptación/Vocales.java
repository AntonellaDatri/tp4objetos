package encriptación;

import java.util.ArrayList;

public class Vocales implements TipoDeEncriptacion {

	@Override
	public String encriptar(String texto) {
		String nuevoTexto = "";
		int tamañoDelTexto = texto.length();
		while (tamañoDelTexto != 0) {
			nuevoTexto = siguente(texto.substring((tamañoDelTexto-1), tamañoDelTexto)) + nuevoTexto;
			tamañoDelTexto = tamañoDelTexto -1;
		}
		return nuevoTexto;
	}

	@Override
	public String desencriptar(String texto) {
		String nuevoTexto = "";
		int tamañoDelTexto = texto.length();
		while (tamañoDelTexto != 0) {
			nuevoTexto = (anterior(texto.substring((tamañoDelTexto-1), tamañoDelTexto))) + nuevoTexto;
			tamañoDelTexto = tamañoDelTexto -1;
		}
		return nuevoTexto;
	}


	public String siguente(String valor) {
		if (valor.equals("A") || valor.equals("a")) {
			return "e";
		}
		else if (valor.equals("E") || valor.equals("e")){
			return "i";
		}
		else if (valor.equals("I") || valor.equals("i")) {
			return "o";
		}
		
		else if (valor.equals("O") || valor.equals("o")) {
			return "u";
		}
		
		else if (valor.equals("U") || valor.equals("u")) {
			return "a";		
		}
		
		else {return valor;}
	}
	
	public String anterior(String valor) {
		if (valor.equals("A") || valor.equals("a")) {
			return "u";
		}
		else if (valor.equals("E") || valor.equals("e")){
			return "a";
		}
		else if (valor.equals("I") || valor.equals("i")) {
			return "e";
		}
		
		else if (valor.equals("O") || valor.equals("o")) {
			return "i";
		}
		
		else if (valor.equals("U") || valor.equals("u")) {
			return "o";		
		}
		else {return valor;}
	}



	
}
