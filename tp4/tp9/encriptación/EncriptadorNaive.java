package encriptación;

public class EncriptadorNaive {
	TipoDeEncriptacion tipo;

	public void setTipo(TipoDeEncriptacion tipo) {
		this.tipo = tipo;
	}
	
	public String encriptar(String texto) {
		return tipo.encriptar(texto);
	}
	public String desencriptar(String texto) {
		return tipo.desencriptar(texto);
	}
	
	
}
