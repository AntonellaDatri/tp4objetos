package mercadoCentral;

import java.util.ArrayList;

public class Caja implements Agencia{
	ArrayList <Producto> productosAdquiridos = new ArrayList<Producto>();
	
	public void registrarProducto(Producto _producto) {
		productosAdquiridos.add(_producto);
		_producto.decrementarStock();
	}
	
	public double montoApagar() {
		int precioTotal = 0;
		for(Producto producto : productosAdquiridos) {
			precioTotal+= producto.getPrecio();
		}
		
		return precioTotal; 
	}

	@Override
	public void registrarPago(Factura factura) {
		factura.montoAPagar();
		factura.registrar();
	}
}
