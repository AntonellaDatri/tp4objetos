package mercadoCentral;

public abstract class Factura {
	
	Agencia agencia;
	 
	public abstract double montoAPagar();
		
	public void registrar() {
		agencia.registrarPago(this);
	}
}
