package mercadoCentral;

public abstract class Producto {
	double precio;
	int stock;

	public abstract double getPrecio() ;
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getStock() {
		return stock;
	}
	
	public void decrementarStock() {
		stock -= 1;
	}
	
	public void incrementarProducto(int cant) {
		stock += cant;
	}
	
}
