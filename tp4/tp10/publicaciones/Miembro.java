package publicaciones;

public class Miembro{
	
	private Sistema sistema;
	private ListaDeTemasDeInv temasDeInvestigacion;
	
	public Miembro(Sistema sistema){
		this.sistema=sistema;
		this.sistema.addObserver(this);
	}

	public void update(Sistema o, ArticulosCientificos arg) {
		temasDeInvestigacion.verificar(arg,this);
		
	}

	public void notificarSi(boolean b, ArticulosCientificos art) {
		if (b) {
			System.out.println("Nuevo Articulo Cientifico que te puede interesar" + art.getTitulo());
		}
	}

	public void notificar(ArticulosCientificos art) {
		System.out.println("Nuevo Articulo Cientifico que te puede interesar" + art.getTitulo());
	}

}
