package publicaciones;

import java.util.List;

public class Sistema {
	List<ArticulosCientificos> articulosCientificos;
	List<Miembro> miembros;
	
	public void agregararArticulosCientificos (ArticulosCientificos art) {
		this.articulosCientificos.add(art);
		this.notificar(art);
	}
	
	public void addObserver(Miembro miembro) {
		this.miembros.add(miembro);
	}

	private void notificar(ArticulosCientificos art) {
			this.setChanged(art);
			this.notifyObservers(art);
		
	}

	private void setChanged(ArticulosCientificos art) {
		articulosCientificos.add(art);		
	}

	private void notifyObservers(ArticulosCientificos art) {
		for (Miembro m : miembros) {
			m.update(this, art);
		}
	}

}
