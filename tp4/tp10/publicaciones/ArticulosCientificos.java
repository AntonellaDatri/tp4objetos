package publicaciones;

import java.util.List;

public class ArticulosCientificos {
	private String titulo;
	private List<String> autores;
	private String filicacion;
	private String tipo;
	private String lugar;
	private List<String> palabrasClave;
	
	public ArticulosCientificos(String titulo, String filicacion, String tipo, String lugar) {
		super();
		this.titulo = titulo;
		this.filicacion = filicacion;
		this.tipo = tipo;
		this.lugar = lugar;
	}
	
	public void agregarAutor(String autor) {
		autores.add(autor);
	}
	
	public void agregarPalabraClave(String pc) {
		palabrasClave.add(pc);
	}
	
	public String getTitulo() {
		return titulo;
	}

	public boolean estaElAutor(String autor) {
		return this.autores.contains(autor);
	}
	
	public boolean esELMismoTipo(String tipo) {
		return this.tipo == tipo;
	}
	
	public boolean esLaMismaFilicacion(String filiacion) {
		return this.filicacion == filiacion;
	}
	
	public boolean esELMismoLugar(String lugar) {
		return this.lugar == lugar;
	}
	public boolean estaPalabrasClaver(String palabrasClave) {
		return this.palabrasClave.contains(palabrasClave);
	}

}
