package publicaciones;

import java.util.List;

public class ListaDeTemasDeInv {
	private List<String> titulos;
	private List<String> autores;
	private List<String> filiaciones;
	private List<String> tipos;
	private List<String> lugares;
	private List<String> palabrasClaves;
	

	public void agregarTitulo(String titulo) {
		this.titulos.add(titulo);
	}
	public void agregarAutor(String autor) {
		this.autores.add(autor);
	}
	public void agregarFilicacion(String filicacion) {
		this.filiaciones.add(filicacion);
	}
	public void agregarTipo(String tipo) {
		this.tipos.add(tipo);
	}	
	public void agregarLugar(String lugar) {
		this.lugares.add(lugar);
	}	
	public void agregarPalabraClave(String pc) {
		this.palabrasClaves.add(pc);
	}
	
	public void contieneTitulo(ArticulosCientificos art, Miembro miembro) {
		if (titulos.contains(art.getTitulo())){
			miembro.notificar(art);
		}
	}
	public void contieneAutor(ArticulosCientificos art, Miembro miembro) {
		List<String> actual = autores;
		while (! (actual.isEmpty()) && (art.estaElAutor(actual.get(0))) ) {
			actual.remove(0);
		}
		notificarSi(actual, art, miembro);
	}
	
	public void contieneFiliacion(ArticulosCientificos art, Miembro miembro) {
		if (titulos.contains(art.getTitulo())){
			miembro.notificar(art);
		}
	}
	public void contieneTipos(ArticulosCientificos art, Miembro miembro) {
		if (titulos.contains(art.getTitulo())){
			miembro.notificar(art);
		}
	}
	public void contieneLugares(ArticulosCientificos art, Miembro miembro) {
		if (titulos.contains(art.getTitulo())){
			miembro.notificar(art);
		}
	}
	public void contienePalabraClave(ArticulosCientificos art, Miembro miembro) {
		List<String> actual = palabrasClaves;
		while (! (actual.isEmpty()) && (art.estaPalabrasClaver(actual.get(0))) ) {
			actual.remove(0);
		}
		notificarSi(actual, art, miembro);
	}
	
	public void notificarSi(List<String> list, ArticulosCientificos art, Miembro miembro) {
		if(! list.isEmpty()) {
			miembro.notificar(art);
		}
	}
	public void verificar(ArticulosCientificos art, Miembro miembro) {
		contieneTitulo(art, miembro);
		contieneAutor(art, miembro);
		contieneFiliacion(art, miembro);
		contieneTipos(art, miembro);
		contieneLugares(art, miembro);
		contienePalabraClave(art, miembro);
		
	}
}
