package encuentroDeportivo;

import java.util.List;

public class AppResultados implements Observer{
	private Servidor sistema;
	private List<String> deportesSuscripto;
	
	@Override
	public void actualizar(EncuentroDeportivo partido) {
		if(deportesSuscripto.contains(partido.getDeporte())) {
			notificar(partido);
		}
	}

	private void notificar(EncuentroDeportivo partido) {
		System.out.println("Nuevo partido de interes" + partido);
		
	}
	
}
