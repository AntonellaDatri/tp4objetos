package encuentroDeportivo;

import java.util.List;

public abstract class Observable {
	List<Observer> observers;
	
	public void notifyObservers(EncuentroDeportivo partido) {
		for(Observer o: observers) {
			o.actualizar(partido);
		}
	}
	
	public void addObserver(Observer obs) {
		this.observers.add(obs);
	}
	public void deleteObserver(Observer obs) {
		this.observers.remove(obs);
	}
	
	
	
}
