package encuentroDeportivo;

import java.util.List;

public class Servidor extends Observable{
	
	List<EncuentroDeportivo> partidos;
	
	public void addPartidos(EncuentroDeportivo partido) {
		this.partidos.add(partido);
		this.notifyObservers(partido);
	}
	
}
