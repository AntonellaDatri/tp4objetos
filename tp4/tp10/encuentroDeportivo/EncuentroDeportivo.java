package encuentroDeportivo;

import java.util.List;

public class EncuentroDeportivo {
	private String resultado;
	private List<String> contrincantes;
	private String Deporte; 
	
	public void addContrincante(String cont) {
		contrincantes.add(cont);
	}

	public List<String> getContrincantes() {
		return contrincantes;
	}

	public String getDeporte() {
		return Deporte;
	}
	
}
