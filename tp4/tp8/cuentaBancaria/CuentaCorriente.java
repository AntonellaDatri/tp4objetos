package cuentaBancaria;

public class CuentaCorriente extends CuentaBancaria {
	private int descubierto;
	public CuentaCorriente(String titular) {
		super(titular);
		// TODO Auto-generated constructor stub
	}

	public int getDescubierto(){
	 	return this.descubierto;
	}

	@Override
	protected boolean condicion(int monto) {
		return this.getSaldo() + this.getDescubierto() >= monto;
	}
}