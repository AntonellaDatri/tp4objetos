package wikipedia;

public class SameInitialLetter extends Filter {

	@Override
	protected boolean correspondeA(Page initialPage, Page page) {
		return initialPage.getTittle().charAt(0) == page.getTittle().charAt(0);
	}

}
