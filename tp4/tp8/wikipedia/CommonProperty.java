package wikipedia;

import java.util.ArrayList;
import java.util.Map;

public class CommonProperty extends Filter {

	@Override
	protected boolean correspondeA(Page initialPage, Page page) {
		ArrayList<String> valores =  (ArrayList<String>) page.getInfobox().keySet();
		while ((! valores.isEmpty()) && (! contiene(initialPage.getInfobox(), valores.get(0)))) {
			valores.remove(0);
		}
		return (! valores.isEmpty());
	}

	private boolean contiene(Map<String, WikipediaPage> infobox, String string) {
		return infobox.containsKey(string);
	}

}
