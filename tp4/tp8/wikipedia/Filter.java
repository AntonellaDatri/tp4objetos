package wikipedia;

import java.util.ArrayList;

public abstract class Filter {
	private ArrayList<Page> pages = new ArrayList<Page>();
	
	public void addPage(Page page) {
		this.pages.add(page);		
	}
	
	public ArrayList<Page> filtrar(Page page){
		ArrayList<Page> filterPages = new ArrayList<Page>();
		for (Page p : pages) {
			 addSiCorresponde(page, p, filterPages);
		}
		return filterPages;
	}

	private ArrayList<Page> addSiCorresponde(Page initialPage, Page page, ArrayList<Page> filterPages) {
		if (correspondeA(initialPage, page)) {
			filterPages.add(page);
		}
		return filterPages;
	}

	protected abstract boolean correspondeA(Page initialPage, Page page);

}
