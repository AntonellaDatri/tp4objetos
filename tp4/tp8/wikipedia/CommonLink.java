package wikipedia;

import java.util.LinkedList;

public class CommonLink extends Filter {

	@Override
	protected boolean correspondeA(Page initialPage, Page page) {
		LinkedList<WikipediaPage> list = (LinkedList<WikipediaPage>) page.getLinks();
		while(! (list.isEmpty()) && (! (initialPage.getLinks().contains(list.getFirst()))));{
			list.remove();
		}
		
		return ! (list.isEmpty());
	}

}
