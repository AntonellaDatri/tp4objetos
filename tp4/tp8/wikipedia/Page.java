package wikipedia;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Page implements WikipediaPage {
	String title;
	List<WikipediaPage> link = new LinkedList<WikipediaPage>();
	Map<String, WikipediaPage> Infobox;
	
	@Override
	public String getTittle() {
		return title;
	}

	@Override
	public List<WikipediaPage> getLinks() {
		return link;
	}

	@Override
	public Map<String, WikipediaPage> getInfobox() {
		return Infobox;
	}

	public Page(String title, List<WikipediaPage> link, Map<String, WikipediaPage> infobox) {
		super();
		this.title = title;
		this.link = link;
		Infobox = infobox;
	}
	
	
}
