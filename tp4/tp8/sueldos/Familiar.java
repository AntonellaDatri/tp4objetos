package sueldos;

public class Familiar extends Empleados {
	int hijos = 0; 
	boolean conyugue = false;
	int sueldoXFamiliar;
	

	@Override
	protected int extra() {
		return (hijos * sueldoXFamiliar) + extraXConyugue();
	}


	private int extraXConyugue() {
		if (conyugue) {
			return sueldoXFamiliar;
		}
		else return 0;
	}
	public void hijos(int hijos) {
		this.hijos = hijos;
	}
	
	public void tieneConjugue() {
		conyugue = true;
	}
	
	public void tuvoUnHijo() {
		hijos += 1;
	}

		public Familiar(int horasTrabajadas, int sueldoBasico, int sueldoXhora, int sueldoXFamiliar) {
		super(horasTrabajadas, sueldoBasico, sueldoXhora);
		this.sueldoXFamiliar = sueldoXFamiliar;
	}

}
