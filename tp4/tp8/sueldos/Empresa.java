package sueldos;

import java.util.ArrayList;

public class Empresa {
	ArrayList <Empleados> empleados = new ArrayList<Empleados>(); 
	
	public void agregarEmpleado(Empleados empleado) {
		this.empleados.add(empleado);
	}
	
	public double pagoDeSueldos() {
		double sueldos = 0;
		for (Empleados e : empleados) {
			sueldos += e.sueldo(); 
		}
		return sueldos;
		
	}
}
