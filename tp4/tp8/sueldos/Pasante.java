package sueldos;

public class Pasante extends Empleados {
	@Override
	protected int extra() {
		return 0;
	}

	public Pasante(int horasTrabajadas) {
		super(horasTrabajadas, 0, 40);
	}

}
