package sueldos;

public abstract class Empleados {
	protected int horasTrabajadas;
	protected int sueldoBasico;
	protected int sueldoXhora;
	
	public double sueldo() {
		return (((horasTrabajadas * sueldoXhora) + sueldoBasico + extra()) * 0.87);
	}

	protected abstract int extra();

	public Empleados(int horasTrabajadas, int sueldoBasico, int sueldoXhora) {
		super();
		this.horasTrabajadas = horasTrabajadas;
		this.sueldoBasico = sueldoBasico;
		this.sueldoXhora = sueldoXhora;
	}

}
