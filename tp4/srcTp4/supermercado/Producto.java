package supermercado;

public class Producto implements Precio {
	String nombre;
	Double precio;
	Boolean esPrecioCuidado = false;
	
	public void perteneceALosPreciosCuidados() {
		esPrecioCuidado = true;
	}

	public String getNombre() {
		return nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public Boolean esPrecioCuidado() {
		return esPrecioCuidado;
	}

	public void aumentarPrecio(Double _precio) {
		precio += _precio;
	}

	public Producto(String nombre, Double precio, Boolean precioCuidado) {
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.esPrecioCuidado = precioCuidado;
	}

	public Producto(String nombre, Double precio) {
		super();
		this.nombre = nombre;
		this.precio = precio;
	}
	
	
}

