package supermercado;

import java.util.ArrayList;

public class Supermercado {
	private String nombre;
	private String direccion;
	private ArrayList <Producto> productos= new ArrayList<Producto>();
	
	
	public Supermercado(String nombre, String direccion) {
		super();
		this.nombre = nombre;
		this.direccion = direccion;
	}
	public void agregarProducto(Producto newProducto) {
		productos.add(newProducto);
	}
	
	public double getPrecioTotal() {
		double precioTotal = 0.0;
		for (Producto producto : productos ) {
		 precioTotal += producto.getPrecio();
		}
		return precioTotal;
	}

	public int getCantidadDeProductos() {
		return productos.size();
	} 
}
