package supermercado;

public class ProductoPrimeraNecesidad extends Producto {
	
	double descuentoAAplicar;

	public ProductoPrimeraNecesidad(String nombre, Double precio, double descuentoAAplicar) {
		super(nombre, precio);
		this.descuentoAAplicar = descuentoAAplicar;
	}

	public ProductoPrimeraNecesidad(String nombre, Double precio,  double descuentoAAplicar, Boolean precioCuidado) {
		super(nombre, precio, precioCuidado);
		this.descuentoAAplicar = descuentoAAplicar;
	}
 
	public double getPrecio() {
		return precio - ((precio * descuentoAAplicar)/100);
		
	}
	

}
