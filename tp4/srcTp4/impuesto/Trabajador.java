package impuesto;

public class Trabajador {
	double totalPercibido;
	double montoImponible;
	double horasExtra;
	
//	public double getTotalPercibido() {
		
	//}
	
	public double getMontoImponible() {
		return montoImponible;
	}
	
	public double getImpuestoAPagar() {
		return montoImponible * 0.2;
	}
}
/*
 
 Trabajador>>getTotalPercibido()
 //retornar el monto total percibido por el trabajador
 
Obra publicada con Licencia Creative Commons Reconocimiento Compartir igual 4.0
Trabajador>>getMontoImponible()
 //retornar el monto imponible al impuesto al trabajador
 
Trabajador>>getImpuestoAPagar()
 //retornar el monto a pagar por el trabajador, calculado como el 2% del monto imponible

 */